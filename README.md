## Tidelift / Bitbucket Example Integration

An example integration between [Bitbucket Pipelines](https://bitbucket.org/product/features/pipelines) and
the [Tidelift Subscription](https://tidelift.com).

Be sure to change `TL_TEAM` and `TL_PROJECT` in the `bitbucket-pipelines.yml` and to also
set `TL_TOKEN` in your _Repository variables_ under _Pipelines_ in your repository settings.

For complete documentation, please see the [Tidelift / Bitbucket Integration](https://tidelift.com/docs/subscriber/bitbucket-integration) page.
